/* ----------------------** Class 1 | Vars **------------------------- */

// var name = 'Leonel',
//     lastname = 'Castellano',
//     age = 28

// console.log(`Hi ${name}, mi age is: ${age}`)






/* ----------------------**  Class 2 |  Vars: String **------------------------- */

// var strUpper = name.toUpperCase(),
//     strLow = lastname.toLowerCase(),
//     fullName = `${name} ${lastname}`

// console.log(strUpper.charAt(0) + ' ' + strLow.length)
// console.log(fullName.substr(0, 3))






/* ----------------------** Class 3 | Vars: Numbers **------------------------- */

// var weight = 68,
//     winePrice = 200.3,
//     total,
//     persons = 2
//     pizzaSlides = 8


// age = age + 1
// weight -= 5

// total = Math.round(winePrice * 100 * 3) / 100

// let totalFixed = total.toFixed(2)

// console.log(pizzaSlides / persons)






/* ----------------------** FClass 4 | Functions **------------------------- */

// var name = 'Leonel',
//     age = 22

//     function showAge(name, age) {
//         console.log(`${name} have ${age} years old`)
//     }

    // showAge(name, age)






/* ----------------------** Class 5 | Scope Functions **------------------------- */

// var name = 'leito'

// function showNameUpperCased(value) {
//     value = value.toUpperCase()
//     console.log(value)
// } 

// showNameUpperCased(name)
// console.log(name)






/* ----------------------** Class 6 | Objects **------------------------- */

// var leonel = {
//     name: 'Leonel',
//     lastname: 'Castellano',
//     age: 28
//     },

//     pedro = {
//         name: 'Pedro',
//         lastname: 'Ruiz',
//         age: 19
//     }

// function showNameUpperCased({ name }) {
//     console.log(name.toUpperCase())
// } 

// showNameUpperCased(leonel)
// showNameUpperCased(pedro)






/* ----------------------** Class 7 | Destructuring Objects **------------------------- */

// var leonel = {
//     name: 'Leonel',
//     lastname: 'Castellano',
//     age: 28
//     },

//     pedro = {
//         name: 'Pedro',
//         lastname: 'Ruiz',
//         age: 19
//     }

// function showNameUpperCased(person) {
//     var { name } = person
//     console.log(name.toUpperCase())
// } 

// showNameUpperCased(leonel)
// showNameUpperCased(pedro)






/* ----------------------** Class 8 | Params like reference or like value **------------------------- */

// var pedro = {
//         name: 'Pedro',
//         lastname: 'Ruiz',
//         age: 19
//     }

// function birthday(person) {
//     person.age ++
// }

// function birthday(person) {
//     return {
//         ...person,
//         age: person.age ++
//     }
// }

// birthday(pedro)






/* ----------------------** Class 9 | Comparisons in Javascript **------------------------- */

var x = 4,
    y = '4'

    console.log("x = 4, y = '4'")
    console.log(`comparison only value  between x == y ${x == y}`)
    console.log(`comparison value and type between x === y ${x === y}`)

var person = {
        name: 'Pepe',
        age: 20
    },
    anotherPerson = { ...person }

    console.log(person === anotherPerson)
    
    // anotherPerson = person
    // console.log(person === anotherPerson)
