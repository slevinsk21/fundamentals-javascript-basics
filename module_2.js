/* ----------------------** Class 1 | Conditionals **------------------------- */

var person = {
        name: 'Leonel',
        lastname: 'Castellano',
        age: 21,
        engineer: true,
        singer: false,
        dj: false,
        drone: true
    }

function showProfesions(person) {
    console.log(`${person.name} is:`)

    if (person.engineer) {
        console.log('Engineer')
    }

    if (person.singer) {
        console.log('Singer')
    }

    if (person.dj) {
        console.log('Dj')
    }

    if (person.drone) {
        console.log('fly drones')
    }
}


// showProfesions(person)







/* ----------------------**  Class 2 |  Functions that return values **------------------------- */

const ADULT_AGE = 18

// function comparisonAge(age) {
//     return age >= ADULT_AGE
// }

// function isadult(person) {
//     let { name, age } = person
//     let response = comparisonAge(age) ? 'an adult' : 'a child'

//     console.log(`${name} is ${response}`)
// }

// isadult(person)








/* ----------------------**  Class 3 |  Arrow Functions **------------------------- */


// comparisonAge = age => age >= ADULT_AGE

// const isadult = person => {
//     let { name, age } = person
//     let response = comparisonAge(age) ? 'an adult' : 'a child'

//     console.log(`${name} is ${response}`)
// }

// isadult(person)

// grantAccess = ({ age }) => {
//     if (!comparisonAge(age)) console.log('Access Denied!!!')
// }

// grantAccess(person)





/* ----------------------**  Class 4 |  Repetitives Strutures (for) **------------------------- */

person.weight = 75

// const HANDLE_WEIGHT = 0.200, DAYS_OF_YEAR = 365

// console.log(`starting the year ${person.name} weight was ${person.weight} kgs`)

// increaseWeight = person => person.weight += HANDLE_WEIGHT
// loseWeight = person => person.weight -= HANDLE_WEIGHT

// for (let i = 1; i <= DAYS_OF_YEAR; i++) {
//     var random = Math.random()

//     if (random < 0.25) {
//         increaseWeight(person)
//     } else if (random < 0.5) {
//         loseWeight(person)
//     }
// }

// console.log(`and finishing the year ${person.name} his weight ${person.weight.toFixed(0)} kgs`)





/* ----------------------**  Class 5 |  Repetitives Strutures (while) **------------------------- */

const GOAL = person.weight - 3,
    HANDLE_WEIGHT = 0.200,
    DAYS_OF_YEAR = 365

increaseWeight = person => person.weight += HANDLE_WEIGHT
loseWeight = person => person.weight -= HANDLE_WEIGHT
eatToMuch = () => Math.random() < 0.3
doSports = () => Math.random() < 0.4

// console.log(`starting the year ${person.name} weight was ${person.weight} kgs`)

let days = 0
while (person.weight > GOAL) {
    // debugger // do breakpoint here in the code

    if (eatToMuch())
        increaseWeight(person)

    if (doSports())
        loseWeight(person)

        days++
}

// console.log(`it was ${days} until ${person.name} slimming 3kg`)





/* ----------------------**  Class 6 |  Repetitives Strutures (do-while) **------------------------- */
var percentage = 10,
    quantity

    getAmountPercentage = value => {
        return value * percentage / 100
    }
    
    getData = () => {
        percentage = prompt("agregar porcentaje %", )
        quantity = prompt("agregar Cantidad", )

        if (percentage && quantity) {
            alert(`la cantidad es ${quantity} menos el ${percentage}% (que es de ${getAmountPercentage(quantity)}) recibes en cash ${quantity - getAmountPercentage(quantity)}`)
        }
    }

    // getData()

    var counter = 0
    const istRain = () => Math.random() < 0.25


    // do {
    //     counter++
    // } while (!istRain())


    // console.log(`I go to see that is rain ${counter} time${counter > 1 ? 's' : ''}` )





/* ----------------------**  Class 67|  Repetitives Strutures (switch) **------------------------- */
var sign = prompt('Which is your sign ?').toLowerCase()

switch (sign) {
    case 'picis':
        console.log(`yo are boring ${sign}`)
        break;

    case 'tauro':
        console.log(`yo are fire ${sign}`)
        break;

    case 'cáncer':
    case 'cancer':
        console.log(`yo are a crap ${sign}`)
        break;
        
    case 'acuario':
        console.log(`yo are water ${sign}`)
        break;
        
    case 'sagitario':
        console.log(`yo are minotaur ${sign}`)
        break;
    
    case 'géminis':
    case 'geminis':
        console.log(`yo are minotaur ${sign}`)
        break;

    default:
        console.log('Eres tremendo mmgvo la cagaste')
        break;
}


// console.log(sign)