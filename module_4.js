/* ----------------------** Classes 1, 2, 3 | How works Classes (protoype) on javascript, Modifing a protorype, context of this **------------------------- */
// function ChildOf(son, father) {
//     var fn = function() {}
//     fn.prototype = father.prototype
//     son.prototype = new fn
//     son.prototype.constructor = son
// }

// function Person(name, lastname, height) {
//     this.name = name,
//     this.lastname = lastname,
//     this.height = height
// }

// Person.prototype.sayHi = function() {
//     console.log(`Hi mi name is ${this.name}`)
// }

// Person.prototype.iAmTall = function () {
//     console.log(`Are you tall? ${this.height >= 1.8 ? 'Yes' : 'Nope...'}`)
//     return this.height > 1.8
// }

// var leonel = new Person('Leonel', 'Castellano', 1.80)
// var rosa = new Person('Rosa', 'Catatumbo', 1.60)
// var jhon = new Person('Jhon', 'Wick', 1.72)

// leonel.iAmTall()
// rosa.iAmTall()
// leonel.iAmTall()

// console.log(leonel)




/* ----------------------** Class 4 | the true hiding about js Classes **------------------------- */


// function Developer(name, lastname) {
//     this.name = name,
//     this.lastname = lastname
// }

// ChildOf(Developer, Person)
// Developer.prototype.sayHi = function() {
//     console.log(`Hi mi name is ${this.name} and I developer`)
// }





/* ----------------------** Class 5 | Classes on js **------------------------- */

class Person {
    constructor(name, lastname, height) {
        this.name = name,
        this.lastname = lastname,
        this.height = height
    }

    sayHi() {
        console.log(`Hi mi name is ${this.name}`)
    }

    iAmTall() {
        console.log(`Are you tall? ${this.height >= 1.8 ? 'Yes' : 'Nope...'}`)
        return this.height >= 1.8
    }
}


var leonel = new Person('Leonel', 'Castellano', 1.80)

class Developer extends Person {
    constructor(name, lastname, height) {
        super(name, lastname, height)
    }

    sayHi() {
        console.log(`Hi mi name is ${this.name} and I developer`)
    }
}