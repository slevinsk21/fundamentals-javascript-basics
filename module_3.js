/* ----------------------** Class 1 | Arrays **------------------------- */

var leonel = {
    name: 'Leonel',
    lastname: 'Castellano',
    age: 21,
    height: 1.60,
    quantityBooks: 54
},
carlos = {
    name: 'Carlos',
    lastname: 'Castillo',
    age: 12,
    height: 1.80,
    quantityBooks: 300
}
vicky = {
    name: 'Vicky',
    lastname: 'Perez',
    age: 16,
    height: 2.10,
    quantityBooks: 50
}
paula = {
    name: 'Paula',
    lastname: 'Londra',
    age: 30,
    height: 1.55,
    quantityBooks: 1
}
pepe = {
    name: 'Pepe',
    lastname: 'Truenno',
    age: 50,
    height: 1.63,
    quantityBooks: 15
}

var persons = [leonel, carlos, vicky, paula, pepe]

// for (let i = 0; i < persons.length; i++) {
//     const person = persons[i];
//     console.log(`${person.name} have ${person.age} years old`)
// }




/* ----------------------** Class 2 | Filtring Arrays **------------------------- */

// const isAdult = ({ age }) => age >= 21
// var adultPersons = persons.filter(isAdult)
// console.log(adultPersons)




/* ----------------------** Class 3 | Transform Arrays **------------------------- */


// const meterToCentimeters = person => ({
//     ...person,
//     height: person.height * 100
// })
// var personsTall = persons.map(meterToCentimeters)
// console.log(personsTall)




/* ----------------------** Class 4 | Reductor Arrays to unique value **------------------------- */
var totalBooks = 0
// for (let i = 0; i < persons.length; i++) {
//     totalBooks += persons[i].quantityBooks
// }

const reducer = (i, { quantityBooks }) => i + person.quantityBooks
totalBooks = persons.reduce(reducer, 0)
console.log(totalBooks)
