/* ----------------------** Class 1 | Functions like params **------------------------- */

class Person {
    constructor(name, lastname, height) {
        this.name = name,
        this.lastname = lastname,
        this.height = height
    }

    greeting(fn) {
        var { name, lastname } = this
        console.log(`Hi mi name is ${name} ${lastname}`)

        if (fn) fn(name, lastname)
    }

    iAmTall() {
        console.log(`Are you tall? ${this.height >= 1.8 ? 'Yes' : 'Nope...'}`)
        return this.height >= 1.8
    }
}


var leonel = new Person('Leonel', 'Castellano', 1.80)

class Developer extends Person {
    constructor(name, lastname, height) {
        super(name, lastname, height)
    }

    greeting(fn) {
        var { name, lastname } = this
        console.log(`Hi mi name is ${name} ${lastname} and I developer`)

        if (fn) fn(name, lastname, true)
    }
}

function returnGreeting(name, lastname, isDev) {
    console.log(`Good morning ${name} ${lastname}`)

    console.log(isDev ? 'whoo you are developer' : '')
}

var carlos = new Developer('Carlos', 'Castro', 2.10)

// leonel.greeting()
// leonel.greeting(returnGreeting)




/* ----------------------** Class 3 | How works the time Js **------------------------- */

// console.log('a')
// setTimeout(() => console.log('b'), 0)
// console.log('c')

// return a c b

// setTimeout(() => console.log('d'), 2000)

// for (let i = 0; i < 10000000000; i++) {
//     const element = i;
    
// }



/* ----------------------** Class 4 | Callbacks **------------------------- */

const API_URL = 'https://swapi.co/api/'
const PEOPLE_URL = 'people/:id'

const requestAPI = async id => {
    const url = API_URL + PEOPLE_URL.replace(':id', id)

    try {
        const response = await fetch(url, { method: 'GET' })
        let data = await response.json()
        console.log("Hi I am " + data.name + ' ' + id)
    } catch (e) {
        console.error(e)
    }
}


// requestAPI(1)



/* ----------------------** Class 5 | Multiples Requests **------------------------- */


// requestAPI(1)
// requestAPI(2)
// requestAPI(3)
// requestAPI(4)
// requestAPI(5)
// requestAPI(6)
// requestAPI(7)





/* ----------------------** Class 6, 7 | Multiples Requests & cach errors Callbacks **------------------------- */

const multipleWithOrder = async () => {
    try {
        await requestAPI(1)
        await requestAPI(2)
        await requestAPI(3)
        await requestAPI(4)
        await requestAPI(5)
        requestAPI(6)

    } catch (e) {
        console.error(e)
    }
}

// multipleWithOrder()



/* ----------------------** Class 8 | Promises **------------------------- */

function requestAPIPromise(id) {
    return new Promise((resolve, reject) => {
        const url = API_URL + PEOPLE_URL.replace(':id', id)
        const config = { method: 'GET' }
        
        fetch(url, config)
            .then(response => response.json())
            .then(response => resolve(response))
            .catch(() => reject(id))
    })
}


/* 
requestAPIPromise(1)
    .then(response => response.json())
    .then(myJson => console.log("Hi I am " + myJson.name))
    .catch(e => console.log(e))
    .finally(() => console.log('Finished!!!')) 
*/


/* ----------------------** Class 9 | Chained Promises  **------------------------- */


/*
requestAPIPromise(1)
    .then(myJson => {
        console.log(`Hi I am ${myJson.name}`)
        
        requestAPIPromise(2)
            .then(myJson => {
                console.log(`Hi I am ${myJson.name}`)

                requestAPIPromise(3)
                    .then(myJson => console.log(`Hi I am ${myJson.name}`))
                    .catch(e => console.log(e))
            })
            .catch(e => console.log(e))
    })
    .catch(e => console.log(e))

 */

/* ----------------------** Class 10 | Multiples Promises on paralel  **------------------------- */

var ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
var promises = ids.map(id => requestAPIPromise(id))
/* 
Promise.all(promises)
    .then(characters => console.log(characters))
    .catch(id => console.error('Failed promise on id:' + id))
 */


/* ----------------------** Class 11 | async-await the last on Asynchronism  **------------------------- */


async function GetCharacters() {
    var ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    var promises = ids.map(id => requestAPIPromise(id))

    try {
        var characters = await Promise.all(promises)
        console.log(characters)
        
    } catch (e) {
        console.error(e)
    }

}


// GetCharacters()